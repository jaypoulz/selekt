###
# selektable.js.coffee
#
# @author Jeremy Poulin
# @date 10/2/15
# @description Defines the Selektable object.
#
# Selektables a wrapper around an object that binds to a SelektPane and acted
# upon by a SelektHandler.
###

class Selektable
  constructor: (@object, @configs = {}) ->
    @configs.selektPanes ?= SelektPanes
    @configs.selektHandlers ?= SelektHandlers
    @configs.selektionTest ?= Selekt.isContainedBy
    @isSelectedBy ?= @configs.selektionTest
    Selektables.push(@)

    for selektPane in @configs.selektPanes
      selectPane.register(@)

  selekt: (event) ->
    if typeof(@configs.selektHandlers) is 'function'
      @configs.selektHandlers(event)

    handlers = @configs.SelektHandlers
    if typeof(handlers) is 'object') and
    Object.prototype.toString.call(handlers) is '[object Array]'
      for handler in handlers
        if typeof(handler) is 'function' then handler()
