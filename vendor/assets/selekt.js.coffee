###
# selekt.js.coffee
#
# @author Jeremy Poulin
# @date 10/2/15
# @description Defines the Selekt utility.
#
# This object performs 3 main tasks.
# 1. Allows for configurations of Selekt-wide options.
# 2. Loads the Selekt objects directly into the browser.
# 3. Signals the ready callback.
###

if $ is 'undefined'
  console.error("Selekt depends on jQuery, which is undefined.")

class Selekt
  contructor: (@configs = {}) ->
    @init()

  init: ->
    @initEventBindings()
    @initGlobals()
    @initSelektBox()
    if configs.runtime? and configs.runtime.analyticsEnabled
      @initAnalytics()

  initEventBindings: ->
    $(window).mouseup (event) ->
      @selektBox.mouseup(event)
      @forwardEventToSelektPanes("mouseup", event, @getSelectRegion())
    $(window).mousedown (event) ->
      @selektBox.mousedown(event)
      @forwardEventToSelektPanes("mousedown", event)
    $(window).mousemove (event) ->
      @selektBox.mousemove(event)
      @forwardEventToSelektPanes("mousemove", event, @getSelektRegion())
    $(window).mouseover (event) ->
      @forwardEventToSelektPanes("mouseover", event)
    $(window).mouseout (event) ->
      @forwardEventToSelektPanes("mouseout", event)
    $(window).mouseenter (event) ->
      @forwardEventToSelektPanes("mouseenter", event)
    $(window).mouseleave (event) ->
      @forwardEventToSelektPanes("mouseleave", event)
    $(window).click (event) ->
      @forwardEventToSelektPanes("click", event)
    $(window).dblclick (event) ->
      @forwardEventToSelektPanes("dblclick", event)

  initGlobals: ->
    window.SelektPanes ?= []
    window.SelektHandlers ?= []
    window.Selektables ?= []

  initSelektbox: ->
    @selektBox = new SelektBox(@configs.selektBox)

  initAnalytics: ->
    # TODO add anaytics

  forwardEventToSelektPanes: (target, event, selektRegion) ->
    for pane in SelektPanes
      if true # TODO add selective event handling
        eval("pane." + target + "(event, selektRegion)")

  class SelektBox
    constructor: (@configs = {}) ->
      @isSelecting = false
      $("body").append("<div id='#selekt-box'></div>")
      @selektBoxHandle = $("#selekt-box")
      @selektBoxElement = selektBoxHandle[0]

    mouseup: (event) ->
      unless @isSelecting
        return

      @selektBoxHandle.hide()
      @isSelecting = false

    mousedown: (event) ->
      @isSelecting = true
      mouse = @getCursor(event)

      @selektBoxElement.origin = mouse
      @selektBoxHandle.css({
        'left': mouse.x + 'px',
        'top': mouse.y + 'px',
        'width': '0',
        'height': '0',
        'transform': ''
      })

      @selektBoxHandle.show()

    mousemove: (event) ->
      unless @isSelecting
        return

      selektRegion = @getSelektRegion()
      origin = selektRegion.origin
      width  = selektRegion.width + 'px'
      height = selektRegion.height + 'px'
      quadrant = @getQuadrant(origin, mouse)
      rotate = 'rotate(0deg)'

      if quadrant is 1 or quadrant is 3
        temp   = width
        width  = height
        height = temp

      switch quadrant
        when 1
          rotate = 'rotate(270deg)'
        when 2
          rotate = 'rotate(180deg)'
        when 3
          rotate = 'rotate(90deg)'

      @selektBoxHandle.css({
        'width': width
        'height': height
        'transform': rotate
      })

    getCursor: (event) ->
      return {
        x: event.pageX
        y: event.pageY
      }

    getQuadrant: (origin, mouse) ->
      if mouse.x >= origin.x and mouse.y < origin.y
        return 1
      if mouse.x < origin.x and mouse.y < origin.y
        return 2
      if mouse.x < origin.x and mouse.y >= origin.y
        return 3
      return 4

    getSelektRegion: ->
      origin   = @selektBoxElement.origin
      mouse    = @getCursor(event)
      quadrant = @getQuadrant(origin, mouse)
      width    = @selektBoxHandle.width()
      height   = @selektBoxHandle.height()

      switch quadrant
        when 1
          origin.y = mouse.y
        when 2
          origin = mouse
        when 3
          origin.x = mouse.x

      return {
        origin: origin
        width: width
        height: height
      }

$ ->
  window.selekt = new Selekt()
