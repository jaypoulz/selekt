###
# selekt-handler.js.coffee
#
# @author Jeremy Poulin
# @date 10/2/15
# @description Defines the SelektHandler object.
#
# SelektHandlers are function wrappers that bind to an event, a SelektPane,
# and call a bunch of Selektables with the function on the triggered event.
###

class SelektHandler
  constructor: (@handler, @configs = {}) ->
    @configs.target ?= {}
    @configs.target.events ?= ['mouseup']
    @configs.target.selektPanes ?= SelektPanes

    SelektHandlers.push(@)
