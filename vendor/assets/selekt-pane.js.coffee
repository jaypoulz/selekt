###
# selekt-pane.js.coffee
#
# @author Jeremy Poulin
# @date 10/2/15
# @description Defines the SelektPane object.
#
# This object is the logical window for selection.
###

class SelektPane
  constructor: (@configs = {}) ->
    @configs.element ?= window
    @configs.width ?= $(@configs.element).width
    @configs.height ?= $(@configs.element).height
    @configs.origin ?= {}
    @configs.origin.x ?= 0
    @configs.origin.y ?= 0
    @selektHandlers ?= []
    SelektPanes.push(@)

  mouseup: (event, selektRegion) ->
    @performCallbacks(event, selektRegion)
    console.log "Something Selekted!"
  mousedown: (event) ->
    @performCallbacks(event, selektRegion)
    console.log "Selektion started."
  mousemove: (event, selektRegion) ->
    @performCallbacks(event, selektRegion)
    console.log "Selekt is selecting..."
  mouseover: (event) ->
    @performCallbacks(event, selektRegion)
  mouseout: (event) ->
    @performCallbacks(event, selektRegion)
  mouseenter: (event) ->
    @performCallbacks(event, selektRegion)
  mouseleave: (event) ->
    @performCallbacks(event, selektRegion)
  click: (event) ->
    @performCallbacks(event, selektRegion)
  dblclick: (event) ->
    @performCallbacks(event, selektRegion)

  performCallbacks: (event, selektRegion) ->
    for selektable in @selektables
      if selektable.isSelektedBy(selektRegion)
        for handler in @selektHandlers
          handler(event)

  register: (handler) ->
    @selektHandlers.push(handler)
